var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req,res){
  res.sendFile(path.join(__dirname, "index.html"));
});

app.post('/', function(res, req){
  res.send("recivido");
});


var clientes = [{'nombre':'iria'},{'nombre':'ruben'},{'nombre':'ruben'}];
//get clientes {}
app.get('/clientes', function(req, res){

  res.json(clientes);
});
//get cliente
app.get('/cliente', function(req, res){
  let cliente=req.param('idcliente');
  res.json(clientes[cliente].nombre);
});
//post mensaje cliente dado de alert-danger
app.post ('/cliente', function(req, res){
  let cliente=req.param('idcliente');
 clientes.add(cliente);
  res.json({ message: 'cliente dado de alta '+ cliente });
});
//put mensaje cliente modificado
app.put ('/cliente', function(req, res){
  console.log(req.param('idcliente'));
  let cliente=req.param('idcliente');
  console.log(cliente);
  res.json({ message: 'cliente modificado ' + clientes[cliente].nombre });
});
//delete mensaje cliente borrado
app.delete ('/cliente', function(req, res){
  let cliente=req.param('idcliente');
  console.log(cliente);
  clientes.splice(cliente,1);
  res.json({ message: 'cliente eliminado'});
});
